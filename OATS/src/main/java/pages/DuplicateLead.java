package pages;

import org.openqa.selenium.WebElement;

import wdMethods.ProjectMethods;

public class DuplicateLead extends ProjectMethods{

	
	public DuplicateLead VerifyDuplicateLeadTitle(String title)  {
		verifyTitle(title);
		return this;
	}
	public ViewLeadPage clickDuplicateCreateLead() {
		WebElement eleDuplicateCreateLead= locateElement("class", "smallSubmit");
		click(eleDuplicateCreateLead);
		return new ViewLeadPage(); 
	}
	
}









