package tests;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import pages.MyHomePage;
import wdMethods.ProjectMethods;

public class TC004_DuplicateLead extends ProjectMethods {
	
	@BeforeClass
	public void setData() {
		testCaseName = "TC004_DuplicateLead";
		testCaseDescription ="Create a lead";
		category = "Smoke";
		author= "Babu";
		dataSheetName="TC004";
	}
	@Test(dataProvider="fetchData")
	public  void duplicateLead(String email, String title, String LeadName) throws InterruptedException   
	{
		new MyHomePage()
		.clickLeads()
		.clickFindLeads()
		.ClickEmailTab()
		.typeEmailInfo(email)
		.clickFindLeadsSearch()
		.GetFirstFindLeadLinkLeadId()
		.clickFirstFindLeadLink()
		.clickDuplicateLead()
		.VerifyDuplicateLeadTitle(title)
		.clickDuplicateCreateLead()
		.VerifyLeadFirstName(LeadName);
		
	}

}
